# MovieApp Application

This project was generated with Angular CLI version 8.3.19

## Set Up

1. git clone https://gitlab.com/acontreraslugo21/angular-movie-app.git
2. cd angular-movie-app
3. npm install
4. ng serve

Note: In case of not having angular installed execute the command [Angular](https://cli.angular.io/).
npm install -g @angular/cli

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).