import { Component, OnInit, NgModule } from '@angular/core';
import { MovieService } from '../services/movie.service';
import {Router} from "@angular/router"

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
 
@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.scss']
})

export class AddMovieComponent implements OnInit {

  addMovieForm: FormGroup;
  coverImage = false;
  submitted = false;

  constructor(
    public movieService: MovieService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.addMovieForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      releaseDate: ['', Validators.required],
      coverImage: [null, [Validators.required]],
    });
  }  

  private coverImageSrc: string = '';

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
    
    this.coverImage = true;
  }

  _handleReaderLoaded(e) {
    let reader = e.target;
    this.coverImageSrc = reader.result;
  }

  get f() { return this.addMovieForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.addMovieForm.invalid) {
      return;
    }

    const objectFormGroup = this.addMovieForm;

    if (!objectFormGroup.invalid) {

    }

    this.movieService.addMovie({
      title: objectFormGroup.get('title').value,
      description: objectFormGroup.get('description').value,
      releaseDate: objectFormGroup.get('releaseDate').value,
      coverImage: this.coverImageSrc
    })

    this.router.navigate(['/home'])
  }

}
