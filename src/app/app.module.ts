import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeMoviesComponent } from './home-movies/home-movies.component';
import { AddMovieComponent } from './add-movie/add-movie.component';

import { MovieService } from './services/movie.service';

import {DatePipe} from '@angular/common';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TopFiveComponent } from './top-five/top-five.component';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    HomeMoviesComponent,
    AddMovieComponent,
    MovieDetailComponent,
    TopFiveComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BsDatepickerModule.forRoot()  
  ],
  providers: [MovieService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
