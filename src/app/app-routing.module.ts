import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeMoviesComponent } from './home-movies/home-movies.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { from } from 'rxjs';
import { TopFiveComponent } from './top-five/top-five.component';


const routes: Routes = [
  { path: 'home', component: HomeMoviesComponent },
  { path: 'add-movie', component: AddMovieComponent },
  { path: 'top-five', component: TopFiveComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
