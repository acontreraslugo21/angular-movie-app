import { Component, OnInit, Input} from '@angular/core';
import { MovieService} from '../services/movie.service'
import { Movie } from '../models/Movie';
import { faTrash } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-home-movies',
  templateUrl: './home-movies.component.html',
  styleUrls: ['./home-movies.component.scss']
})
export class HomeMoviesComponent implements OnInit  {
  @Input() movie: Movie[];
  
  selectedMovie: Movie;

  movies: Movie[];

  faTrash = faTrash;
  
  constructor(
    public movieService: MovieService
  ) { }

  onSelect(movie: Movie){
    this.selectedMovie = movie;
  }

  deleteMovie(movie: Movie){
    if(confirm('Are you sure do you want to delete this movie?')){
      this.movieService.deleteMovie(movie);
    }
  }

  ngOnInit() {
   this.movies = this.movieService.getMovies();
  }

}
