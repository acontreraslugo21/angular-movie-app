export interface Movie {
    title: string;
    description: string;
    releaseDate: string;
    coverImage: string;
}