export class MoviesTF{
    constructor(
        public title: string,
        public release: string,
        public description: string,
        public image: string,
    ){}
}