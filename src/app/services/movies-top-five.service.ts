import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { MoviesTF } from '../models/movies-top-five';
import { URL } from '../services/global';

  

@Injectable({
  providedIn: 'root'
})

export class MoviesTopFiveService {
  public url: string;

  constructor(
    public http: Http
  ) {
    this.url = URL.url;
  }

  getMovies(){
    return this.http.get(this.url).map(res => res.json());
  }

}
