import { Component, OnInit } from '@angular/core';
import { MoviesTopFiveService } from '../services/movies-top-five.service';
import { MoviesTF } from '../models/movies-top-five';

@Component({
  selector: 'app-top-five',
  templateUrl: './top-five.component.html',
  styleUrls: ['./top-five.component.scss'],
  providers: [MoviesTopFiveService]
})
export class TopFiveComponent implements OnInit {

  public movies: MoviesTF[];

  constructor(
    private MovieTopFiveService: MoviesTopFiveService
  ) { }

  ngOnInit() {
    this.MovieTopFiveService.getMovies().subscribe(
      result => {
        
        if(result.code != 200 ){
          this.movies = result.movies;
        }else{
          console.log(result);
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
